var API_URL = '';

var app = angular.module('app', ['ngMaterial', 'ng-mfb']);

// Initialize application
app.run(['$rootScope', '$window', '$location', 'facebookService',
    function ($rootScope, $window, $location, facebookService) {

        API_URL = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/api"

        $window.fbAsyncInit = function () {
            // Executed when the SDK is loaded
            FB.init({

                /*
                 The app id of the web app;
                 To register a new app visit Facebook App Dashboard
                 ( https://developers.facebook.com/apps/ )
                 */

                appId: $location.host().contains('localhost') > 0 ? '105004999861317' : '104932839868533',

                /*
                 Adding a Channel File improves the performance
                 of the javascript SDK, by addressing issues
                 with cross-domain communication in certain browsers.
                 */

                channelUrl: '../app/channel.html',

                /*
                 Set if you want to check the authentication status
                 at the start up of the app
                 */

                status: true,

                /*
                 Enable cookies to allow the server to access
                 the session
                 */

                cookie: true,

                /* Parse XFBML */

                xfbml: true
            });

            facebookService.watchLoginChange();
        };

        // Use IIFE to load Facebook SDK
        (function (d) {
            // load the Facebook javascript SDK
            var js,
                id = 'facebook-jssdk',
                ref = d.getElementsByTagName('script')[0];

            if (d.getElementById(id)) {
                return;
            }

            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";

            ref.parentNode.insertBefore(js, ref);
        }(document));

    }
]);

// Initialize services
app.factory('facebookService', ['$http', '$location',
    function ($http, $location) {

        var loggedUser = {isLogged: false};

        var watchLoginChange = function (callback) {
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {

                    loggedUser.isLogged = true;

                    loggedUser.accessToken = response.authResponse.accessToken;
                    getMe(loggedUser);

                    // TODO Create session on backend
                }
                else {
                    loggedUser = {isLogged: false};

                    // TODO Destroy session on backend
                }

                if(callback)
                    callback(loggedUser);

            });
        };

        var getMe = function (user) {
            FB.api('/me', function (res) {
                user._id = res.id;
                user.name = res.name;

                // Send user data to backend
                $http.post(API_URL + '/user', user);

                FB.api("/" + user._id + "/picture", function (response) {
                    if (response && !response.error) {
                        user.pictureUrl = response.data.url;
                    }
                    else{
                        user.pictureUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/public/img/identicon.png"
                    }
                });
            });
        };

        var doLogin = function (callback) {
            FB.login(function (response) {
                watchLoginChange(callback);
            }, {scope: 'user_posts'});
        };

        var doLogout = function (callback) {
            FB.logout(function (response) {
                watchLoginChange(callback);
            });
        };

        var getLoggedUser = function(){
            return loggedUser;
        };

        return {
            watchLoginChange: watchLoginChange,
            doLogin: doLogin,
            doLogout: doLogout,
            getMe: getMe,
            getLoggedUser: getLoggedUser
        }

    }]);

// Initialize controllers
app.controller('MainController', ['$scope', '$rootScope',
    function ($scope, $rootScope) {
        // Nothing for now.
    }]);

app.controller('AuthController', ['$scope', 'facebookService',
    function ($scope, facebookService) {

        // Enable FB login
        $scope.fbLogin = function () {
            facebookService.doLogin(function(loggedUser){
                $scope.loggedUser = loggedUser;
            });
        };

        // Enable FB logout
        $scope.fbLogout = function () {
            facebookService.doLogout(function(loggedUser){
                $scope.loggedUser = loggedUser;
            });
        };

    }
]);
