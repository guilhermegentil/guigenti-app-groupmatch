var services = {
  "language_translation": [
    {
      "name": "Language Translation-jx",
      "label": "language_translation",
      "plan": "standard",
      "credentials": {
        "url": "https://gateway.watsonplatform.net/language-translation/api",
        "username": "9ac52cd0-135c-4972-a8d2-1cae8d7212af",
        "password": "IlK9V42nvYac"
      }
    }
  ],
  "personality_insights": [
    {
      "name": "Personality Insights-u3",
      "label": "personality_insights",
      "plan": "standard",
      "credentials": {
        "url": "https://gateway.watsonplatform.net/personality-insights/api",
        "username": "74ff3eca-70b5-4689-90c4-88ba2f5d646c",
        "password": "Q2yuhjmx9UfX"
      }
    }
  ],
  "cloudantNoSQLDB": [
    {
      "name": "Cloudant NoSQL DB-af",
      "label": "cloudantNoSQLDB",
      "plan": "Shared",
      "credentials": {
        "username": "9c23c868-816e-476f-9989-a44be6b31679-bluemix",
        "password": "794bc58120d71ca4d495d829719d2ce6695a9992f72db5d54f46a553cf66d2bd",
        "host": "9c23c868-816e-476f-9989-a44be6b31679-bluemix.cloudant.com",
        "port": 443,
        "url": "https://9c23c868-816e-476f-9989-a44be6b31679-bluemix:794bc58120d71ca4d495d829719d2ce6695a9992f72db5d54f46a553cf66d2bd@9c23c868-816e-476f-9989-a44be6b31679-bluemix.cloudant.com"
      }
    }
  ],
  "AdvancedMobileAccess": [
    {
      "name": "Mobile Client Access-2x",
      "label": "AdvancedMobileAccess",
      "plan": "Gold",
      "credentials": {
        "serverUrl": "https://imf-authserver.ng.bluemix.net/imf-authserver",
        "clientId": "4c24038f-98a6-4a0c-8736-45da4210d83a",
        "secret": "C1e2vcUjQLyHRAgox28KJw",
        "tenantId": "4c24038f-98a6-4a0c-8736-45da4210d83a",
        "admin_url": "https://mobile.ng.bluemix.net/imfmobileplatformdashboard/?appGuid=4c24038f-98a6-4a0c-8736-45da4210d83a"
      }
    }
  ],
  "SessionCache-1.0": [
    {
      "name": "Session Cache-m3",
      "label": "SessionCache-1.0",
      "plan": "starter",
      "credentials": {
        "catalogEndPoint": "23.246.200.85:2809,23.246.200.84:2809",
        "restResource": "http://192.155.245.74/resources/datacaches/HSJUbYIdRC2q2QzIWfkaewTS",
        "restResourceSecure": "https://ecaas32.ng.bluemix.net/resources/datacaches/HSJUbYIdRC2q2QzIWfkaewTS",
        "gridName": "HSJUbYIdRC2q2QzIWfkaewTS",
        "username": "lXpMBxWyRkGIDIsKj0RCRQTB",
        "password": "NbyCPmo3Rm6cpcqdxv2jKgRR"
      }
    }
  ]
};

exports.getServices = function(){
    return services;
}