/**
 * Service for Watson related functionality
 *
 * @author Guilherme G. Amaral
 */
var watson = require('watson-developer-cloud');

var cfenv = require('cfenv');

var servicesConfig = require('../config/services');

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

var watson_language_translation_username = appEnv.services.language_translation && appEnv.services.language_translation.length > 0 ?
    appEnv.services.language_translation[0].credentials.username : servicesConfig.getServices().language_translation[0].credentials.username;
var watson_language_translation_password = appEnv.services.language_translation && appEnv.services.language_translation.length > 0 ?
    appEnv.services.language_translation[0].credentials.password : servicesConfig.getServices().language_translation[0].credentials.password;

var watson_personality_username = appEnv.services.personality_insights && appEnv.services.personality_insights.length > 0 ?
    appEnv.services.personality_insights[0].credentials.username : servicesConfig.getServices().personality_insights[0].credentials.username;
var watson_personality_password = appEnv.services.personality_insights && appEnv.services.personality_insights.length > 0 ?
    appEnv.services.personality_insights[0].credentials.password : servicesConfig.getServices().personality_insights[0].credentials.password;

var watson_api_version = 'v2';

var language_translation = watson.language_translation({
    username: watson_language_translation_username,
    password: watson_language_translation_password,
    version: watson_api_version
});

var personality_insights = watson.personality_insights({
    username: watson_personality_username,
    password: watson_personality_password,
    version: watson_api_version
});

/**
 * Gets a Post, identifies the language it is written and translates it to english
 *
 * @param post
 *     The Post object
 * @param postCount
 * @param callback
 *     Callback function to handle error
 */
exports.getTranslationFor = function (post, postCount, callback) {

    // Identify language and use te correct translation model
    language_translation.identify({text: post.message}, function (err, identifiedLanguages) {
        console.log("Checking language......");
        if (err) {
            console.log("Checking failed. " + JSON.stringify(err));
            callback(err);
        }
        else {

            // get the first, most confident answer
            var lang = identifiedLanguages.languages[0].language.split('-')[0];
            console.log(lang);

            language_translation.translate({text: post.message, source: lang, target: 'en'}, function (err, res) {
                console.log("Translating......");
                if (err) {
                    console.log("Translation failed. " + JSON.stringify(err));
                    callback(err, post, postCount);
                }
                else {
                    console.log("Translated from: " + post.message);
                    post.message = '';
                    for (var i = 0; i < res.translations.length; i++) {
                        post.message += res.translations[i].translation + ' ';
                    }
                    console.log("Translated to: " + post.message);
                    callback(null, post, postCount);
                }
            });
        }
    });

};

exports.getPersonalityInsight = function (data, callback) {
    personality_insights.profile(data, callback);
};