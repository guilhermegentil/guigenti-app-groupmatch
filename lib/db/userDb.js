/**
 * Database access module for Users
 *
 * @author Guilherme G. Amaral
 */

var cloudant = require('./dbConfig');

var DBNAME = "groupmatch-users";

var db = cloudant.getDb().use(DBNAME);

/**
 * Save a document in the specified database
 *
 * @param obj
 *     User to be saved
 * @param callback(err)
 *     Callback function to handle errors
 */
exports.persist = function (obj, callback) {
    var _self = this;

    // Create a new user or update an existing one
    _self.find(obj._id, function(err, data) {
        var updating = false;
        if(!err) {
            obj._rev = data._rev;
            updating = true;
            console.log("User " + obj._id + " already exists. It will be updated");
        }
        
        // Save document
        db.insert({
            name: obj.name, 
            accessToken: obj.accessToken,
            _rev: obj._rev,
            _id: obj._id
        }, function (err, body, header) {
            if (err)
                console.log('[' + DBNAME + '.' + 'insert' + ']', err.message);
            else 
                console.log(updating ? "User " + obj._id + " updated" : "New user " + obj._id + " added to [" + DBNAME + "] database");

            callback(err);
        });

    });
};

/**
 * Find a User by _id in the database
 *
 * @param _id
 *     The User _id
 * @param callback(err, data)
 *     Callback function to handle errors or the received document
 */
exports.find = function (_id, callback) {

    // Get document by _id
    db.get(_id, function (err, data) {

        if (err) 
            console.log('[' + DBNAME + '.' + 'get' + ']', err.message);

        callback(err, data);
    });
    
};

