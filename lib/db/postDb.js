/**
 * Database access module for Posts
 *
 * @author Guilherme G. Amaral
 */

var cloudant = require('./dbConfig');

var DBNAME = "groupmatch-users-personality-insight";

var db = cloudant.getDb().use(DBNAME);

/**
 * Save a document in the specified database
 *
 * @param obj
 *     Post to be saved
 * @param callback(err)
 *     Callback function to handle errors
 */
exports.persist = function (obj, callback) {

    // Save document
    db.insert(obj, function (err, body, header) {

        if (err)
            console.log('[' + DBNAME + '.' + 'insert' + ']', err.message);
        else
            console.log('New object added to [' + DBNAME + '] database');

        callback(err);
    });
};

/**
 * Find a all Posts of a User
 *
 * @param _id
 *     The User _id
 * @param callback(err, data)
 *     Callback function to handle errors or the received document
 */
exports.find = function (_id, callback) {
    
    // Get document by _id
    db.get(_id, function (err, data) {

        if (err) 
            console.log('[' + DBNAME + '.' + 'get' + ']', err.message);

        callback(err, data);
    });
    
};