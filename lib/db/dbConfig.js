var cfenv = require('cfenv');

var servicesConfig = require('../config/services');

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

// Load the Cloudant library.
var Cloudant = require('cloudant');

var url = appEnv.services.cloudantNoSQLDB && appEnv.services.cloudantNoSQLDB.length > 0 ?
    appEnv.services.cloudantNoSQLDB[0].credentials.url : servicesConfig.getServices().cloudantNoSQLDB[0].credentials.url;

// Initialize Cloudant
var cloudant = Cloudant(url);

/**
 *
 * @returns {*}
 */
exports.getDb = function () {
    return cloudant.db;
}