/**
 * Controller for User related functionality
 *
 * @author Guilherme G. Amaral
 */

var db = require('../db/userDb');

/**
 * Save an User object in the database
 *
 * @param user
 *     The user object with the following format:
 *         {
 *           _id: "id_as_string",
 *           access_token: "the_facebook_access_token",
 *           [other_optional_fields]
 *         }
 *
 * @param callback
 *     The callback function to handle errors
 */
exports.save = function (user, callback) {
    db.persist(user, callback);
};

/**
 * Finds one User by it's _id
 * @param _id
 *     The User _id as it is on Facebook
 * @param callback(err, data)
 *     The callback function to handle errors and the returned data
 */
exports.find = function (_id, callback) {
    db.find(_id, callback);
};
