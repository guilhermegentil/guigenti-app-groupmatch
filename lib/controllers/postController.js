/**
 * Controller for Posts related functionality
 *
 * @type {*|exports|module.exports}
 * @author Guilherme G. Amaral
 */
var request = require('request');

var db = require('../db/postDb');
var watson = require('../services/watsonService');

/**
 * Save post in the database
 *
 * @param post
 *     The post to be saved
 * @param callback
 */
exports.save = function (post, callback) {
    db.persist(post, callback);
};

/**
 * Find the Posts on one User on Facebook
 *
 * @param user
 *     the User
 * @param callback
 *     Callback function to handle errors and responses
 */
exports.findPostsOnFacebook = function (user, callback) {
    console.log("access_token: " + user.accessToken);
    var url = "https://graph.facebook.com/v2.5/" + user._id + "/posts?limit=1000&access_token=" + user.accessToken;
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {

            var res = JSON.parse(body);
            var data = res.data;
            var posts = [];

            for (var i = 0; i < data.length; i++) {
                var post = {};

                // Get stories that does not contains the user name
                if (data[i].message) {
                    post.userId = user._id;
                    post.message = data[i].message;

                    posts.push(post);
                }
            }

            callback(undefined, posts);
        }
        else {
            callback(error, []);
        }
    })
};


/**
 * Find the Posts of a User and store it on the database
 *
 * @param user
 *     The user
 */
exports.findAndStorePosts = function (user) {
    var _self = this;

    // Find the posts
    _self.findPostsOnFacebook(user, function (err, data) {
        console.log("Searching posts from user " + user._id);

        if (err) {
            console.log("Failed to retrieve posts for user " + user._id);
            console.log(err.message);
        }
        else {
            console.log("Posts: " + JSON.stringify(data));

            var insightData = {_id: user._id, rawText: ''};

            for (var i = 0; i < data.length; i++) {
                // Translate the post message
                watson.getTranslationFor(data[i], i, function (err, post, count) {

                    if (err)
                        console.log("Failed to use Watson Translate service on post " + count + ". " + err.error_message);
                    else
                        insightData.rawText += post.message + ' ';

                    // Check if finished processing all posts
                    if (count == data.length - 1) {

                        // Save text and personality insght, call watson personality insight service
                        watson.getPersonalityInsight({text: insightData.rawText, language: 'en'}, function(err, profileData){
                            if(err) {
                                console.log("Error calling Watson personality insight. ", err.message)
                            }
                            else {
                                insightData.profile = profileData;

                                _self.save(insightData, function (err) {
                                    if (err)
                                        console.log("Failed to persist insight data. " + err.message);
                                });
                            }

                        });

                    }
                });
            }

        }
    });
};

/**
 *
 */
exports.find = function(_id, callback) {
    db.find(_id, callback);
}
