/**
 * Endpoints for User related functionality
 *
 * @type {*|exports|module.exports}
 * @author Guilherme G. Amaral
 */
var express = require('express');

var userController = require('../controllers/userController');
var postController = require('../controllers/postController');

var API_ROOT = '/user';
var API_TEST_MSG = API_ROOT + " endpoint is working!";

var router = express.Router();

/**
 * GET /user/test
 *     Test endpoint
 */
router.route(API_ROOT + '/test')
    .get(function (req, res) {
        console.log(JSON.stringify(req));

        res.json({message: API_TEST_MSG});
    });

/**
 * GET /user/:id
 *     Returns a user given its _id
 */
router.route(API_ROOT + '/:id')
    .get(function (req, res) {
        console.log(JSON.stringify(req));

        var _id = req.params.id;

        userController.find(_id, function (err, data) {
            if (err)
                res.status(404).json({code: 404, message: err.message});
            else
                res.json(data);
        });
    });

/**
 * POST /user
 *     Adds the user object to the Users db
 */
router.route(API_ROOT)
    .post(function (req, res) {
        var user = req.body;

        console.log(JSON.stringify(user));

        // persist on database
        userController.save(user, function (err) {
            if (err) {
                res.status(500).json({code: 500, message: "Error during persistence, " + err.message});
            }
            else {
                postController.find(user._id, function(err, data){
                    if(err || !data)
                        postController.findAndStorePosts(user);
                    else
                        console.log("User " + user._id + " already has insight data on database. Skiping data fetch phase...")
                })
                res.status(200).json({code: 200, message: "OK"});
            }
        });
    });

module.exports = router;